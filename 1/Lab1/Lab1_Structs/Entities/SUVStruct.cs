﻿using Lab1.Enums;
using Lab1.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Entities
{
    public struct SUVStruct : IMovable
    {
        private static int _count;
        public int HorsePowers { get; set; }

        public int Count { get => _count; }
        public VehicleTypes VehicleType { get; set; }

        public string Manufacturer { get; set; }
        public int Price { get; set; }

        internal void PerformSignal()
        {
            Console.WriteLine("Beep");
        }
        public void AddHorsePowers(ref SUVStruct suv)
        {
            suv.HorsePowers += this.HorsePowers;
        }
        public void Move()
        {
            Console.WriteLine("The car is moving!");
        }

        public SUVStruct(string manufacturer, int price)
        {
            VehicleType = VehicleTypes.Car;
            Manufacturer = manufacturer;
            Price = price;
            HorsePowers = 200;
            _count++;
        }
        static SUVStruct()
        {
            _count = 0;
        }

        public static implicit operator SUVStruct(int x)
        {
            return new SUVStruct(x.ToString(), x);
        }

        public static explicit operator int(SUVStruct suv)
        {
            return suv.Count;
        }

        public void Deconstruct(out string suvManufacturer, out int suvPrice)
        {
            suvManufacturer = Manufacturer;
            suvPrice = Price;
        }

        public override string ToString()
        {
            return $"This SUV is manufactured by {this.Manufacturer} and costs {this.Price}$";
        }

        public class ManufacturedAt
        {
            public Guid SUVId { get; set; }
            public DateTime CreatedAt { get; private set; }

            public ManufacturedAt()
            {
                CreatedAt = DateTime.Now.AddYears(2);
                SUVId = Guid.NewGuid();
            }
        }

    }
}
