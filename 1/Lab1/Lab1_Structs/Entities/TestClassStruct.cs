﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Entities
{
    public struct TestClassStruct
    {
        public string Manufacturer { get; set; }
        public int Price { get; set; }
        public int ID { get; set; }
        public void Foo() => Console.WriteLine("Hello");
        public void Bar() => Console.WriteLine("World");

        public TestClassStruct(string manufacturer, int price, int id)
        {
            Manufacturer = manufacturer;
            Price = price;
            ID = id;
        }
    }
}
