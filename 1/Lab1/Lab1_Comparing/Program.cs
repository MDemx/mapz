﻿using Lab1.Entities;
using System;
using System.Collections.Generic;
using System.Diagnostics;

long size = 1000000;
Stopwatch stopWatch = new Stopwatch();

/*List<SUV> lst1 = new List<SUV>();
List<SUVStruct> lst2 = new List<SUVStruct>();
List<TestClass> lst3 = new List<TestClass>();
List<TestClassStruct> lst4 = new List<TestClassStruct>();

List<object> ilst = new List<object>();
List<object> dlst = new List<object>();
List<object> flst = new List<object>();
*/
Console.WriteLine("Creating base class");
stopWatch.Start();

for (int i = 0; i < size; i++)
{
    Car basic_class = new Car(100);
}

stopWatch.Stop();

Console.WriteLine(stopWatch.ElapsedMilliseconds);

stopWatch.Restart();


Console.WriteLine("Creating 1 inherited class");
stopWatch.Start();

for (int i = 0; i < size; i++)
{
    SUV inherited_class = new SUV("Toyota", 200);
}

stopWatch.Stop();

Console.WriteLine(stopWatch.ElapsedMilliseconds);

stopWatch.Restart();



Console.WriteLine("Creating 2 inherited class");
stopWatch.Start();

for (int i = 0; i < size; i++)
{
    Inherited3 inherited_class = new Inherited3();
}

stopWatch.Stop();

Console.WriteLine(stopWatch.ElapsedMilliseconds);

stopWatch.Restart();



Console.WriteLine("Creating 3 inherited class");
stopWatch.Start();

for (int i = 0; i < size; i++)
{
    Inherited4 inherited_class = new Inherited4();
}

stopWatch.Stop();

Console.WriteLine(stopWatch.ElapsedMilliseconds);

stopWatch.Restart();



Console.WriteLine("Base class destructor");
stopWatch.Start();

for (int i = 0; i < size; i++)
{
    using (Car basic_class = new Car(100)) { }
}

stopWatch.Stop();

Console.WriteLine(stopWatch.ElapsedMilliseconds);

stopWatch.Restart();



Console.WriteLine("Inherited 1 class destructor");
stopWatch.Start();

for (int i = 0; i < size; i++)
{
    using (SUV inherited_class = new SUV("Toyota", 200)) { }
}

stopWatch.Stop();

Console.WriteLine(stopWatch.ElapsedMilliseconds);

stopWatch.Restart();



Console.WriteLine("Inherited 2 class destructor");
stopWatch.Start();

for (int i = 0; i < size; i++)
{
    using (Inherited3 inherited_class = new Inherited3()) { }
}

stopWatch.Stop();

Console.WriteLine(stopWatch.ElapsedMilliseconds);

stopWatch.Restart();



Console.WriteLine("Inherited 3 class destructor");
stopWatch.Start();

for (int i = 0; i < size; i++)
{
    using (Inherited4 inherited_class = new Inherited4()) { }
}

stopWatch.Stop();

Console.WriteLine(stopWatch.ElapsedMilliseconds);

stopWatch.Restart();

