﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Entities
{
    public class SUV : Car
    {
        private static int _count;

        public int Count { get => _count; }

        public string Manufacturer { get; set; }
        public int Price { get; set; }

        protected int weight = 12000;

        public SUV(): base(1000)
        {
            Manufacturer = "Default";
            Price = 10000;
            _count;
        }

        public SUV(string manufacturer, int price)
            : base(200)
        {
            Manufacturer = manufacturer;
            Price = price;
            _count++;
        }

        static SUV()
        {
            _count = 0;
        }

        public static implicit operator SUV(int x)
        {
            return new SUV(x.ToString(), x);
        }

        public static explicit operator int(SUV suv)
        {
            return suv.Count;
        }

        public void Deconstruct(out string suvManufacturer, out int suvPrice)
        {
            suvManufacturer = Manufacturer;
            suvPrice = Price;
        }

        public override string ToString()
        {
            return $"This SUV is manufactured by {this.Manufacturer} and costs {this.Price}$";
        }

        public class ManufacturedAt
        {
            public Guid SUVId { get; set; }
            public DateTime CreatedAt { get; private set; }

            public ManufacturedAt()
            {
                CreatedAt = DateTime.Now.AddYears(2);
                SUVId = Guid.NewGuid();
            }
        }

    }
}
