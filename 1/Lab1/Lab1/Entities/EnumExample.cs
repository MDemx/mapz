﻿using Lab1.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Entities
{
    internal class EnumExample
    {
        public VehicleTypes FirstType { get; set; }
        public VehicleTypes SecondType { get; set; }

        public EnumExample(VehicleTypes t1, VehicleTypes t2)
        {
            FirstType = t1;
            SecondType = t2;
        }

        public void ShowRes()
        {
            Console.WriteLine($"Operator & result is {FirstType & SecondType}");
            Console.WriteLine($"Operator | result is {FirstType | SecondType}");
            Console.WriteLine($"Operator ^ result is {FirstType ^ SecondType}");
        }
    }
}
