﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Entities
{
    internal static class BoxingTest
    {
        public static void ShowRes(object o1, object o2)
        {
            Console.WriteLine(o1.ToString() + " " + o2.ToString());
            var h1 = (SUV)o1;
            var h2 = (SUV)o2;
            h1.PerformSignal();
            h2.PerformSignal();
        }
    }
}
