﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Entities
{
    public abstract class Vehicle
    {
        public abstract int HorsePowers { get; set; }
        private string signal = "Beep";

        public void PerformSignal()
        {
            Console.WriteLine(signal);
        }

        public abstract void SetHorsePowers(ref Vehicle vehicle);
    }
}
