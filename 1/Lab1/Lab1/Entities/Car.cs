﻿using Lab1.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Entities
{
    public class Car : Vehicle, IMovable, IDisposable
    {
        private int _horsePowers;
        public override int HorsePowers {
            get
            {
                return _horsePowers;
            }
            set 
            { 
                _horsePowers = value;
            }
        }

        public void Move()
        {
            Console.WriteLine("The car is moving!");
        }

        public Car(int horsePowers)
        {
            this.HorsePowers = horsePowers;
            Dispose(false);
        }

        private bool disposed;
        private void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing) {}

                this.disposed = true;
            }
        }

        public override string ToString()
        {
            return $"This car has {HorsePowers} horse powers";
        }

        public override void SetHorsePowers(ref Vehicle vehicle)
        {
            vehicle.HorsePowers += this.HorsePowers;
        }

        void IDisposable.Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~Car()
        {
            Dispose(false);
        }
    }
}
