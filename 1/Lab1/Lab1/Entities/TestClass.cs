﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Entities
{
    public class TestClass
    {
        public string Manufacturer { get; set; }
        public int Price { get; set; }
        public int ID { get; set; }
        public void Foo() => Console.WriteLine("Hello");
        public void Bar() => Console.WriteLine("World");

        public TestClass(string manufacturer, int price, int id)
        {
            Manufacturer = manufacturer;
            Price = price;
            ID = id;
        }
    }
}
