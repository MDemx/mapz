﻿using Lab1.Entities;
using Lab1.Interfaces;
using System;

Console.WriteLine("Start");

#region
Console.WriteLine("\nUnboxing... \n");
SUV suv1 = new SUV("Toyota", 20000);
SUV suv2 = new SUV("Volvo", 45000);
BoxingTest.ShowRes(suv1, suv2);
#endregion

#region
EnumExample enum1 = new EnumExample(Lab1.Enums.VehicleTypes.Airplane, Lab1.Enums.VehicleTypes.Car);
enum1.ShowRes();
#endregion

#region
SUV suv3 = new SUV("Test", 100000);
SUV.ManufacturedAt sc = new SUV.ManufacturedAt();
#endregion  

#region
SUV suv4= new SUV("Toyota", 20000);
(string manufacturer, int price) = suv4;
Console.WriteLine($"This SUV is manufactured by {manufacturer} and costs {price}$");

Console.WriteLine("\nImplicit...");
int x = 3;
suv4 = (SUV)x;
Console.WriteLine(suv4.ToString());


Console.WriteLine("\nExplicit...");
int y = ((int)suv4);
Console.WriteLine(y);


Car car1 = new Car(500);
Vehicle vehicle1 = new Car(200);
car1.SetHorsePowers(ref vehicle1);
Console.WriteLine(vehicle1);
#endregion

