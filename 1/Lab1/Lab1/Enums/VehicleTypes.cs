﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Enums
{
    internal enum VehicleTypes
    {
        Airplane = 1,
        Car = 2,
        Motorcycle = 3
    }
}
