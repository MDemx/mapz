﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.Interfaces
{
    public interface ITradeDepartment
    {
        void AddPlanetTrades(IPlanetTrades company);
    }
}
