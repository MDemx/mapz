﻿    using Lab3.Interfaces;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    namespace Lab3.Entities
    {
        public class TradeDepartment : ITradeDepartment
        {
            private static TradeDepartment _instance = null;

            private List<IPlanetTrades> _planetTradesList;

            public void AddPlanetTrades(IPlanetTrades planetTrades)
            {
                _planetTradesList.Add(planetTrades);
            }

            protected TradeDepartment() {}

            public static TradeDepartment Init()
            {
                Console.WriteLine("Init");

                if (_instance == null)
                {
                    _instance = new TradeDepartment();
                }

                return _instance;
            }
        }
    }
