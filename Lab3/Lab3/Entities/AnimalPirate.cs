﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.Entities
{
    public class AnimalPirate : AbstractPirate
    {
        public override string? ToString()
        {
            return "I am an animal pirate";
        }
    }
}
