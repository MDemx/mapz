﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.Entities
{
    public class Galaxy
    {
        private AbstractPirate _pirate;
        private AbstractPlanet _planet;

        public Galaxy(AbstractFactory factory)
        {
            _pirate = factory.CreatePirate();
            _planet = factory.CreatePlanet();
        }

        public override string? ToString()
        {
            return $"pirate - {_pirate}\n planet - {_planet}";
        }
    }
}
