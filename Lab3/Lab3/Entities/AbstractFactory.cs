﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.Entities
{
    public abstract class AbstractFactory
    {
        public abstract AbstractPirate CreatePirate();
        public abstract AbstractPlanet CreatePlanet();
    }
}
