﻿using Lab3.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.Entities
{
    public class BattleShip
        : IShip
    {
        private int _weapons;
        public BattleShip(int weapons)
        {
            _weapons = weapons;
        }

        public IShip Clone()
        {
            return new BattleShip(this._weapons);
        }

        public void GetInfo()
        {
            Console.WriteLine("Ship weapons number is {0}", _weapons);
        }
    }
}
