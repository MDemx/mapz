﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.Entities
{
    public class GiantPlanet : AbstractPlanet
    {
        public override string? ToString()
        {
            return "It's a giant planet";
        }
    }
}
