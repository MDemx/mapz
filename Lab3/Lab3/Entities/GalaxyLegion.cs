﻿using Lab3.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.Entities
{
    public class GalaxyLegion
    {
        private IShip _ship;

        public GalaxyLegion(IShip ship)
        {
            _ship = ship.Clone();
        }

        public void ShowGalaxyLegionInfo()
        {
            _ship.GetInfo();
        }
    }
}
