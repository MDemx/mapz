﻿using Lab3.Entities;
using Lab3.Interfaces;

#region singleton
Console.WriteLine("Singleton start");

ITradeDepartment department = TradeDepartment.Init();

Console.WriteLine("Singleton end \n\n");
#endregion

#region  abstract_factory
Console.WriteLine("Abstract factory start");

AbstractFactory firstFactory = new FirstItemFactory();
AbstractFactory secondFactory = new SecondItemFactory();
Galaxy c1 = new Galaxy(secondFactory);
Galaxy c2 = new Galaxy(firstFactory);
Console.WriteLine(c1);
Console.WriteLine(c2);

Console.WriteLine("Abstract factory end\n\n");
#endregion

#region prototype
Console.WriteLine("Prototype start");

IShip battleShip = new BattleShip(20);
GalaxyLegion galaxyLegion = new GalaxyLegion(battleShip);
galaxyLegion.ShowGalaxyLegionInfo();

Console.WriteLine("Prototype end");
#endregion
