﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2.BLL.Entities
{
    public class Example : IComparer<Example>, IComparable<Example>
    {
        public int ID { get; private set; }
        public string CreatedBy { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public int ProjectID { get; set; }

        public Example(int id, string createdBy, string description, DateTime createdAt, int projectID)
        {
            ID = id;
            CreatedBy = createdBy;
            Description = description;
            CreatedAt = createdAt;
            ProjectID = projectID;
        }

        public override string? ToString()
        {
            return $"Project {ID} created by {CreatedBy} - {ProjectID}";
        }

        public int Compare(Example? x, Example? y)
        {
            if (x.CreatedAt > y.CreatedAt)
            {
                return 1;
            }
            else if (x.CreatedAt < y.CreatedAt)
            {
                return -1;
            }
            else
            {
                return 0;
            }
        }

        public int CompareTo(Example? other)
        {
            return this.Compare(this, other);
        }
    }
}
