﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2.BLL.Entities
{
    public static class ListExtended
    {
        public static void PrintToConsole<T>(this List<T> list)
        {
            Console.WriteLine(String.Join(Environment.NewLine, list));
        }

        public static string GetStringFormat<T>(this List<T> list)  
        {
            StringBuilder string_builder = new StringBuilder();

            foreach (var item in list)
            {
                string_builder.Append(item.ToString());
                string_builder.AppendLine(Environment.NewLine);
            }

            return string_builder.ToString();
        }
    }
}
