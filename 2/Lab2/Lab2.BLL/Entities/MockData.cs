﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2.BLL.Entities
{
    public class MockData
    {
        public List<Project> Projects
        {
            get
            {
                return new List<Project>()
                { 
                    new Project("Project1", "Dev1", "description1", 1),
                    new Project("Project2", "Dev2", "description2", 2),
                    new Project("Project3", "Dev3", "description3", 3),
                    new Project("Project4", "Dev4", "description4", 4),
                    new Project("Project5", "Dev5", "description5", 5), 
                    new Project("Project6", "Dev6", "description6", 6),
                    new Project("Project7", "Dev7", "description7", 7)

                };
            }
        }

        public List<Example> Examples
        {
            get
            {
                return new List<Example>()
                {
                    new Example(1, "aDev1", "description1", DateTime.Now, 1),
                    new Example(2, "bDev2", "description2", DateTime.Now, 2),
                    new Example(3, "cDev3", "description3", DateTime.Now, 3),
                    new Example(4, "dDev4", "description4", DateTime.Now, 4),
                    new Example(5, "eDev5", "description5", DateTime.Now, 5),
                    new Example(6, "fDev6", "description5", DateTime.Now, 6),
                    new Example(7, "gDev7", "description5", DateTime.Now, 7),
                };
            }
        }

    }
}
