﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2.BLL.Entities
{
    public static class AdditionalStructures
    {
        public static Queue<string> QueueString(List<Example> list, string str)
        {
            var queue = new Queue<string>();
            var res = list
                .OrderBy(p => p.CreatedBy)
                .Select(l => l.CreatedBy)
                .Where(p => p.StartsWith(str));
            
            foreach (var item in res)
            {
                queue.Enqueue(item);
            }

            return queue;
        }
    }
}
