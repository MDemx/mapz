using Lab2.BLL.Entities;

namespace Lab2
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var mockData = new MockData();

            foreach (var item in mockData.Projects)
            {
                var str = string.Format("Id: {0}, Name: {1}", item.ID, item.Name) + Environment.NewLine;
                textBox1.AppendText(str);
            }

            // IComparer sort
            var sortedTasks = mockData.Examples.OrderBy(p => p.ID);

            foreach (var item in sortedTasks)
            {
                var str = string.Format("Dev name: {0}, ProjectID: {1}", item.CreatedBy, item.ProjectID) + Environment.NewLine;
                textBox2.AppendText(str);
            }

            var d1 = new { res = DictionaryMaker.ProjectsDictionary(mockData.Projects) };
            var d2 = DictionaryMaker.InfoDictionary(mockData.Examples);

            var q1 = AdditionalStructures.QueueString(mockData.Examples, "t");

            textBox3.AppendText(String.Join(Environment.NewLine, d1.res.Select(l => l.Key.ToString() + " " + l.Value.ToString()).ToArray()));
            textBox3.AppendText(Environment.NewLine);
            textBox3.AppendText(Environment.NewLine);
            textBox3.AppendText(String.Join(Environment.NewLine, d2.Select(l => l.Key.ToString() + " " + String.Join(", ", l.Value))));

        }
    }
}