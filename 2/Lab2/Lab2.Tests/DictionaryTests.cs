﻿using Lab2.BLL.Entities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2.Tests
{
    [TestFixture]
    public class DictionaryTests
    {
        private MockData _mock;

        [SetUp]
        public void TestSetup()
        {
            _mock = new MockData();
        }

        [Test]
        public void DictinaryMaker_CollectionIsReturned()
        {
            var res = DictionaryMaker.ProjectsDictionary(_mock.Projects);
            Assert.That(res, Is.Not.Empty);
        }

        [Test]
        [TestCase(123)]
        [TestCase(321)]
        [TestCase(333)]
        [TestCase(99)]
        public void DictinaryMaker_UnexistingIdNotReturned(int id)
        {
            var res = DictionaryMaker.ProjectsDictionary(_mock.Projects);
            Assert.That(!res.ContainsKey(id));
        }
    }
}
