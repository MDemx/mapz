﻿using Lab2.BLL.Entities;
using NUnit.Framework;

namespace Lab2.Tests
{
    [TestFixture]
    public class AdditionalStructuresTests
    {
        private MockData _mock;

        [SetUp]
        public void TestSetup()
        {
            _mock = new MockData();
        }

        [Test]
        [TestCase("q")]
        [TestCase("w")]
        [TestCase("z")]
        [TestCase("j")]
        public void AdditionalStructures_EmptyCollectionReturned(string str)
        {
            var result = AdditionalStructures.QueueString(_mock.Examples, str);
            Assert.That(result, Is.Empty);
        }

        [Test]
        [TestCase("a")]
        [TestCase("b")]
        [TestCase("c")]
        public void AdditionalStructures_CollectionReturned(string str)
        {
            var result = AdditionalStructures.QueueString(_mock.Examples, str);
            Assert.That(result, Is.Not.Empty);
        }

    }
}