﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5.Entities
{
    public class PirateShip : EnemyShip
    {
        protected override void FindPlanet()
        {
            Console.WriteLine("Find defined planet");
        }

        protected override void GoToPlanet()
        {
            Console.WriteLine("Go to defined planet");
        }

        protected override void PrepareGuns()
        {
            Console.WriteLine("Prepare all guns");
        }
    }
}
