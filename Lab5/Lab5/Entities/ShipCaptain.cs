﻿using Lab5.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5.Entities
{
    public class ShipCaptain : IObservable
    {
        private List<IObserver> _observers = new List<IObserver>();

        public void AddObserver(IObserver o)
        {
            _observers.Add(o);
        }

        public void NotifyObservers(string msg)
        {
            foreach (IObserver o in _observers)
            {
                o.Update(msg);
            }
        }

        public void RemoveObserver(IObserver o)
        {
            _observers.Remove(o);
        }
    }
}
