﻿using Lab5.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5.Entities
{
    public class Ship : Mediator, IObserver
    {
        private string _msg;
        public CrewMember Captain { get; set; }
        public CrewMember Engineer { get; set; }

        public override void Send(CrewMember crewMember)
        {
            if (Captain == crewMember)
                Captain.Notify("Ship is repaired!");
            else
                Engineer.Notify("Repair the ship!");
        }

        public void Update(string message)
        {
            _msg = message;
            Console.WriteLine("Captain says: " + _msg);
        }
    }
}
