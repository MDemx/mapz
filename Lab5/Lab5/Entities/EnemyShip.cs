﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5.Entities
{
    public class EnemyShip
    {
        protected virtual void FindPlanet()
        {
            Console.WriteLine("Default find planet");
        }

        protected virtual void PrepareGuns()
        {
            Console.WriteLine("Default prepare guns");
        }

        protected virtual void GoToPlanet()
        {
            Console.WriteLine("Default go to planet");
        }

        public void AttackPlanet()
        {
            FindPlanet();
            PrepareGuns();
            GoToPlanet();
        }
    }
}
