﻿using Lab5.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5.Entities
{
    public class Captain : CrewMember
    {
        public Captain(Mediator mediator) 
            : base(mediator)
        {
        }
        public override void Notify(string message)
        {
            Console.WriteLine("The crew says: " + message);
        }

        public override void Send()
        {
            Console.WriteLine("Command given!");
            mediator.Send(this);
        }
    }
}
