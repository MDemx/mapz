﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5.Entities
{
    public class Engineer : CrewMember, Interfaces.IObserver
    {
        private string _msg;

        public Engineer(Mediator mediator) 
            : base(mediator)
        {
        }

        public override void Notify(string message)
        {
            Console.WriteLine("Command to engineer: " + message);
        }

        public override void Send()
        {
            Console.WriteLine("Repair the ship!");
            mediator.Send(this);
        }

        public void Update(string message)
        {
            _msg = message;
            Console.WriteLine("Command: " + _msg);
        }
    }
}
