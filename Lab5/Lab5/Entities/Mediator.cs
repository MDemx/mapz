﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5.Entities
{
    public abstract class Mediator
    {
        public abstract void Send(CrewMember colleague);
    }
}
