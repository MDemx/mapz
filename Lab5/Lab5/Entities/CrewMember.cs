﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5.Entities
{
    public abstract class CrewMember
    {
        protected Mediator mediator;

        public CrewMember(Mediator mediator)
        {
            this.mediator = mediator;
        }

        public abstract void Send();

        public abstract void Notify(string message);
    }
}
