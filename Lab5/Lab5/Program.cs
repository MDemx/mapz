﻿using Lab5.Entities;
using Lab5.Interfaces;

#region mediator
Console.WriteLine("Mediator start\n");

Ship ship = new Ship();
CrewMember captain = new Captain(ship);
CrewMember engineer = new Engineer(ship); 

ship.Captain = captain;
ship.Engineer = engineer;

engineer.Send();
captain.Send();

Console.WriteLine("Mediator end\n");
#endregion

#region observer
Console.WriteLine("Observer start\n");

IObservable shipCaptain = new ShipCaptain();

Engineer engineer2 = new Engineer(ship);

shipCaptain.AddObserver(engineer2);
shipCaptain.AddObserver(ship);
shipCaptain.NotifyObservers("Repair the ship");

Console.WriteLine("Observer end\n");
#endregion

#region template
Console.WriteLine("Template method start\n");

EnemyShip ship2 = new PirateShip();
ship2.AttackPlanet();

Console.WriteLine("Template method end\n");
#endregion