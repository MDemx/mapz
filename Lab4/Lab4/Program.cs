﻿using Lab4.Entities;

#region facade
Console.WriteLine("Facade start\n");

ShipCommander shipCommander = new ShipCommander();
Facade facade = new Facade(new Planet(), new Ship());
shipCommander.RepairShip(facade);

Console.WriteLine("Facade end\n\n");
#endregion

#region decorator
Console.WriteLine("Decorator start\n");

Pirate pirate = new VeneraPirate();
Console.WriteLine(pirate.OriginPlanet);

pirate = new MarsPirate(pirate);
Console.WriteLine(pirate.OriginPlanet);

Console.WriteLine("Decorator end\n\n");
#endregion

#region composite
Console.WriteLine("Composite start\n");

Galaxy galaxy = new Galaxy("Main galaxy");

Galaxy galaxy1 = new Galaxy("Galaxy1");
galaxy1.Add(new TradedItem("2 ships"));
galaxy1.Add(new TradedItem("50 new technologies"));

Galaxy galaxy2 = new Galaxy("Galaxy2");
galaxy2.Add(new TradedItem("9 engines"));

Galaxy galaxy3 = new Galaxy("Galaxy3");
galaxy3.Add(new TradedItem("1000kg of synthetic meat"));

galaxy.Add(galaxy1);
galaxy.Add(galaxy2);
galaxy.Add(galaxy3);

galaxy.Print();

Console.WriteLine("Composite end\n\n");
#endregion