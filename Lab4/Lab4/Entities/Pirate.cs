﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.Entities
{
    internal abstract class Pirate
    {
        public string OriginPlanet { get; protected set; }

        public Pirate(string originPlanet)
        {
            this.OriginPlanet = originPlanet;
        }
        public abstract int GetAge();
    }
}
