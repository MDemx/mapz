﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.Entities
{
    internal class Facade
    {
        private Planet _planet;  
        private Ship _ship;

        public Facade(Planet planet, Ship ship)
        {
            _planet = planet;
            _ship = ship;
        }

        public void Start()
        {
            _planet.Find();
            _ship.GoToPlanet();
            _ship.Repair();
        }

        public void Stop()
        {
            _ship.LeavePlanet();
        }
    }
}
