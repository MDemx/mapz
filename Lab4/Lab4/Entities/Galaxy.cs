﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.Entities
{
    internal class Galaxy : Component
    {
        private List<Component> _tradedItems;

        public Galaxy(string name) 
            : base(name)
        {
            _tradedItems = new List<Component>();
        }

        public void Add(Component tradedItem) 
        {
            _tradedItems.Add(tradedItem);
        }

        public void Remove(Component tradedItem)
        {
            _tradedItems.Remove(tradedItem);
        }

        public override void Print()
        {
            Console.WriteLine("Galaxy: " + Name);
            Console.WriteLine("Galaxy subnodes: ");

            foreach (var tradedItem in _tradedItems)
            {
                tradedItem.Print();
            }
        }
    }
}
