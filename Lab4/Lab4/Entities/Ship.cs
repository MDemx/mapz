﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.Entities
{
    internal class Ship
    {
        public void GoToPlanet()
        {
            Console.WriteLine("Going to planet...");
        }
        public void Repair()
        {
            Console.WriteLine("Ship repaired");
        }

        public void LeavePlanet()
        {
            Console.WriteLine("Ship is in the space");
        }
    }
}
