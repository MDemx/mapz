﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.Entities
{
    internal class MarsPirate : PirateDecorator
    {
        public MarsPirate(Pirate p)
            : base(p.OriginPlanet, p)
        { 
        
        }

        public override int GetAge()
        {
            Console.WriteLine("Get age");
            return pirate.GetAge();
        }
    }
}
