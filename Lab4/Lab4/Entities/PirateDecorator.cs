﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.Entities
{
    internal abstract class PirateDecorator : Pirate
    {
        protected Pirate pirate;
        public PirateDecorator(string originPlanet, Pirate pirate) 
            : base(originPlanet)
        {
            this.pirate = pirate;
        }

    }
}
