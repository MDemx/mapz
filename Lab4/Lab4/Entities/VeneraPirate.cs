﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.Entities
{
    internal class VeneraPirate : Pirate
    {
        public VeneraPirate() 
            : base("Venera")
        { 

        }

        public override int GetAge()
        {
            return 42;
        }
    }
}
